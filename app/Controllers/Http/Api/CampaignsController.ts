import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import MyHelper from 'App/Helper/MyHelper'
import Campaign from 'App/Models/Campaign'
import Donation from 'App/Models/Donation'

export default class CampaignsController {
    public async index({ request }: HttpContextContract) {
        const params = request.all()

        params.page = params.page ?? 1;
        params.limit = params.limit ?? 10;

        let data = Campaign.query().preload('categories').preload('sumDonations').orderBy('id', 'desc')
        if (params.q) {
            data = data.where((query) => {
                query.where('title', 'ilike', '%' + params.q + '%')
                query.where('description', 'ilike', '%' + params.q + '%')
            })
        }
        const campaign = await data.paginate(params.page, params.limit)
        return await MyHelper.sendResponse(200, { campaigns: campaign })

    }

    public async show({ request }: HttpContextContract) {

        const params = request.params()

        const campaign = await Campaign.query().preload('categories').preload('sumDonations').where('slug', params.slug).first()

        const donation = await Donation.query().preload('donaturs').where('status', 'success').limit(10).orderBy('id', 'desc').where('campaign_id', campaign!!.id)

        if (!campaign) {
            return await MyHelper.sendResponse(404)
        }

        return await MyHelper.sendResponse(200, { campaigns: campaign, donations: donation })
    }


}
