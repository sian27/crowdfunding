import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import MyHelper from 'App/Helper/MyHelper'
import Category from 'App/Models/Category'

export default class CategoriesController {

    public async index() {

        const category = await Category.all()

        return await MyHelper.sendResponse(200, { categories: category })
    }

    public async show({ request }: HttpContextContract) {
        const params = request.params()

        const category = await Category.query().where('slug', params!!.slug).preload('campaigns', (query) => {
            query.preload('sumDonations')
        }).first()

        if (!category) {
            return await MyHelper.sendResponse(404)
        }

        return await MyHelper.sendResponse(200, { categories: category })
    }
}
