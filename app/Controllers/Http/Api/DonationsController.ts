import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import MyHelper from 'App/Helper/MyHelper'
import Campaign from 'App/Models/Campaign'
import Donation from 'App/Models/Donation'
import Env from '@ioc:Adonis/Core/Env'

export default class DonationsController {

    public async index({ request, auth }: HttpContextContract) {
        const params = request.all()

        params.page = params.page ?? 1;
        params.limit = params.limit ?? 10;

        const data = await Donation.query().preload('campaings').where('donatur_id', auth.use('api').user!.id).orderBy('id', 'desc').paginate(params.page, params.limit)

        return await MyHelper.sendResponse(200, { donations: data })
    }

    public async store({ request, auth }: HttpContextContract) {

        const post = request.body()
        const { MidtransClient } = require('midtrans-node-client')
        // Create Snap API instance
        const snap = new MidtransClient.Snap({
            isProduction: false,
            serverKey: Env.get('MIDTRANS_SERVER_KEY'),
            clientKey: Env.get('MIDTRANS_CLIENT_KEY')
        })

        // Alogrithm Create Invoice
        const length = 10;
        let random = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < length; i++) {
            random += characters.charAt(Math.floor(Math.random() *
                characters.length));
        }

        const invoice = 'TRX-' + random.toUpperCase();

        const parameter = {
            "transaction_details": {
                "order_id": invoice,
                "gross_amount": post.amount
            },
            'customer_details': {
                'first_name': await auth.use('api').user!!.name,
                'email': await auth.use('api').user!!.email,
            }
        };

        const trx = await Database.transaction()
        try {
            const donation = new Donation
            const campaign = await Campaign.query().where('slug', post.campaignSlug).first()
            donation.invoice = invoice
            donation.campaignId = campaign!!.id
            donation.amount = post.amount
            donation.pray = post.pray
            donation.status = 'pending'
            snap.createTransaction(parameter)
                .then(async (transaction) => {
                    // transaction token
                    donation.snapToken = transaction.token
                    console.log(transaction.token);
                    await donation.save()
                    await trx.commit()
                    return await MyHelper.sendResponse(200, { token: donation.snapToken })
                }).catch(async () => {
                    await trx.rollback()
                    return await MyHelper.sendResponse(400)
                })
            // snap.createTransaction(parameter).then(console.log).catch(console.error)
        } catch (error) {
            console.log(error);
            await trx.rollback()
            return await MyHelper.sendResponse(400)

        }
    }

}
