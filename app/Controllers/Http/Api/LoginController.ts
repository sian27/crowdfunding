import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Env from '@ioc:Adonis/Core/Env'
import AppsUser from 'App/Models/apps/AppsUser';
import Donatur from 'App/Models/Donatur';
import MyHelper from 'App/Helper/MyHelper';

export default class LoginController {

    public async login({ request, auth }: HttpContextContract) {
        const post = request.body();

        if (!post.type) {
            const validator = schema.create({
                email: schema.string({}, [
                    rules.email()
                ]),
                password: schema.string({}, [
                    rules.minLength(8),
                ])
            })
            await request.validate({ schema: validator })

            const email = post.email
            const password = post.password

            const token = await auth.use('api').attempt(email, password);
            return await MyHelper.sendResponse(200, token.toJSON())
        }
        else if (post.type == 'mobile') {
            const validator = schema.create({
                id: schema.string({}, [
                    rules.minLength(16)
                ]),
                type: schema.string({}, [
                ])
            })
            await request.validate({ schema: validator })

            const crypto = require('crypto')
            const key = Env.get('KEY_AES')
            const iv = Env.get("IV_KEY")
            const plaintext = post.id

            // const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
            // let encryptedData = cipher.update(plaintext, "utf8", "hex");
            // encryptedData += cipher.final("hex");
            // console.log('encrypt', encryptedData);


            // const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
            // let decrypted = decipher.update(encryptedData, 'hex', 'utf8');
            //  decrypted += decipher.final('utf8');
            //  console.log('decrypt', decrypted);

            let id: string = "0"

            try {
                let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
                let decrypted = decipher.update(plaintext, 'hex', 'utf8');
                decrypted += decipher.final('utf8');
                id = decrypted;
            } catch (error) {
                return await MyHelper.sendResponse(400)
            }


            const apps = await AppsUser.query().where('us_id', id).first()
            if (apps) {
                const exist = await Donatur.query().where('apps_id', apps.usId).first()
                if (!exist) {
                    const data = new Donatur
                    data.name = apps.usName
                    data.email = apps.usEmail
                    await data.save()

                    const token = await auth.use('api').generate(data)
                    return await MyHelper.sendResponse(200, token.toJSON())
                }
                const token = await auth.use('api').generate(exist)
                return await MyHelper.sendResponse(200, token.toJSON())
            }
            else {
                return await MyHelper.sendResponse(400)
            }

        }
    }

    public async me({ auth }: HttpContextContract) {

        return await MyHelper.sendResponse(200, { user: auth.use('api').user })
    }

    public async register({ request }: HttpContextContract) {
        const post = request.body()
        
        const validator = schema.create({
            name: schema.string({}, [
                rules.minLength(3)
            ]),
            email: schema.string({}, [
                rules.email()
            ]),
            password: schema.string({}, [
                rules.minLength(8),
            ]),
            repassword: schema.string({}, [
                rules.equalTo(post.password)
            ]),
        })
        await request.validate({ schema: validator })
    }

    public async logout({ auth }: HttpContextContract) {
        await auth.use('api').revoke()

        return await MyHelper.sendResponse(200)
    }
}
