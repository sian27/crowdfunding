// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import MyHelper from 'App/Helper/MyHelper'
import Slider from 'App/Models/Slider'

export default class SlidersController {

    public async index() {
        const slider = await Slider.query().limit(5).orderByRaw('random()')

        return await MyHelper.sendResponse(200, { sliders: slider })
    }
}
