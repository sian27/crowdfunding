import HttpContext from '@ioc:Adonis/Core/HttpContext'

export default class MyHelper {

    static async sendResponse(status: number, data: Object | Array<any> | null = null, message: string | null = null) {
        const response = await HttpContext.get()!!.response
        if (status == 200) {
            return response.ok({
                success: true,
                message: message ?? "Your request has been successfully",
                data: data
            })
        }
        else if (status == 400) {
            return response.badRequest({
                success: false,
                message: message ?? "Something went wrong",
                data: data
            })
        }
        else if (status == 404) {
            return response.notFound({
                success: false,
                message: message ?? "Your request could not be found",
                data: data
            })
        }
    }
}