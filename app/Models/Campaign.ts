import { DateTime } from 'luxon'
import { BaseModel, beforeCreate, beforeDelete, beforeUpdate, column, computed, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Donation from './Donation'
import Category from './Category'
import Env from '@ioc:Adonis/Core/Env'

import HttpContext from '@ioc:Adonis/Core/HttpContext'

export default class Campaign extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string

  @column()
  public slug: string

  @column()
  public categoryId: number

  @column()
  public targetDonation: number

  @column()
  public maxDate: Date

  @column()
  public description: string

  @column()
  public image: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime | null;

  @column()
  public createdBy?: number | null

  @column()
  public updatedBy?: number | null

  @column()
  public deletedBy?: number | null

  @computed()
  public get imageLink() {
    if (this.image && !this.image.includes('http')) {
      return Env.get('URL').replace('0.0.0.0', '127.0.0.1') + 'uploads/campaigns/' + this.image
    }
  }

  @hasOne(() => Donation, {
    onQuery(query) {
      query.select('campaign_id').sum('amount as amount').where('status', 'success').groupBy('donations.campaign_id')
    },
    localKey: "id",
    foreignKey: "campaignId"
  })
  public sumDonations: HasOne<typeof Donation>;

  @hasOne(() => Category, {
    localKey: "categoryId",
    foreignKey: "id"
  })
  public categories: HasOne<typeof Category>

  @beforeCreate()
  public static async assignCreateBy(data: Campaign) {
    const auth = await HttpContext.getOrFail().auth
    data.createdBy = auth.use('api').user!!.id    
  }

  @beforeUpdate()
  public static async assignUpdateBy(data: Campaign) {
    const auth = await HttpContext.getOrFail()!!.auth
    data.updatedBy = auth.use('api').user!!.id
  }

  @beforeDelete()
  public static async assignDeleteBy(data: Campaign) {
    const auth = await HttpContext.getOrFail()!!.auth
      data.deletedBy = auth.use('api').user!!.id
  }
}
