import { DateTime } from 'luxon'
import { BaseModel, beforeCreate, beforeDelete, beforeUpdate, column, computed, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'
import Campaign from './Campaign'

import HttpContext from '@ioc:Adonis/Core/HttpContext'

export default class Category extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public slug: string

  @column()
  public image: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime | null;

  @column()
  public createdBy?: number | null

  @column()
  public updatedBy?: number | null

  @column()
  public deletedBy?: number | null

  @computed()
  public get imageLink() {
    if (this.image && !this.image.includes('http')) {
      return Env.get('URL').replace('0.0.0.0', '127.0.0.1') + 'uploads/categories/' + this.image
    }
  }

  @hasMany(() => Campaign, {
    localKey: "id",
    foreignKey: "categoryId"
  })
  public campaigns: HasMany<typeof Campaign>;

  @beforeCreate()
  public static async assignCreateBy(data: Category) {
    const auth = await HttpContext.getOrFail().auth
    data.createdBy = auth.use('api').user!!.id    
  }

  @beforeUpdate()
  public static async assignUpdateBy(data: Category) {
    const auth = await HttpContext.getOrFail()!!.auth
    data.updatedBy = auth.use('api').user!!.id
  }

  @beforeDelete()
  public static async assignDeleteBy(data: Category) {
    const auth = await HttpContext.getOrFail()!!.auth
      data.deletedBy = auth.use('api').user!!.id
  }
}
