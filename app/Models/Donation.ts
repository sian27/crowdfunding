import { DateTime } from 'luxon'
import { BaseModel, beforeCreate, beforeDelete, beforeUpdate, column, HasOne, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Donatur from './Donatur'
import Campaign from './Campaign'

import HttpContext from '@ioc:Adonis/Core/HttpContext'

export default class Donation extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public invoice: string

  @column()
  public campaignId: number

  @column()
  public donaturId: number

  @column()
  public amount: number

  @column()
  public pray?: string | null

  @column()
  public snapToken?: string

  @column()
  public status: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime | null;

  @column()
  public createdBy?: number | null

  @column()
  public updatedBy?: number | null

  @column()
  public deletedBy?: number | null

  @hasOne(() => Campaign, {
    localKey: "campaignId",
    foreignKey: "id"
  })
  public campaings: HasOne<typeof Campaign>

  @hasOne(() => Donatur, {
    localKey: "donaturId",
    foreignKey: "id"
  })
  public donaturs: HasOne<typeof Donatur>

  @beforeCreate()
  public static async assignCreateBy(data: Donation) {
    const auth = await HttpContext.getOrFail().auth
    data.createdBy = auth.use('api').user!!.id
    data.donaturId = auth.use('api').user!!.id
  }

  @beforeUpdate()
  public static async assignUpdateBy(data: Donation) {
    const auth = await HttpContext.getOrFail()!!.auth
    data.updatedBy = auth.use('api').user!!.id
  }

  @beforeDelete()
  public static async assignDeleteBy(data: Donation) {
    const auth = await HttpContext.getOrFail()!!.auth
      data.deletedBy = auth.use('api').user!!.id
  }
}
