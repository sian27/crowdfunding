import { DateTime } from 'luxon'
import { BaseModel, beforeSave, column, computed } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'
import Hash from '@ioc:Adonis/Core/Hash'

export default class Donatur extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public appsId?: number

  @column()
  public name: string

  @column()
  public email: string

  @column()
  public emailVerifiedAt?: string

  @column()
  public password: string

  @column()
  public image: string

  @column()
  public rememberToken: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime | null;

  @column()
  public createdBy?: number | null

  @column()
  public updatedBy?: number | null

  @column()
  public deletedBy?: number | null

  @beforeSave()
  public static async hashPassword(donatur: Donatur) {
    if (donatur.$dirty.password) {
      donatur.password = await Hash.make(donatur.password)
    }
  }

  // @beforeCreate()
  // public static async assignCreateBy(donatur: Donatur) {
  //   if (auth) {
  //     donatur.createdBy = auth.id
  //   }
  //   donatur.createdBy = null
  // }

  // @beforeUpdate()
  // public static async assignUpdateBy(donatur: Donatur) {
  //   if (auth) {
  //     donatur.updatedBy = auth.id
  //   }
  //   donatur.updatedBy = null
  // }

  // @beforeDelete()
  // public static async assignDeleteBy(donatur: Donatur) {
  //   if (auth) {
  //     donatur.deletedBy = auth.id
  //   }
  //   donatur.deletedBy = null
  // }

  @computed()
  public get imageLink() {
    if (this.image && !this.image.includes('http')) {
      return Env.get('URL').replace('0.0.0.0', '127.0.0.1') + 'uploads/donaturs/' + this.image
    }
  }
}
