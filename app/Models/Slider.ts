import { DateTime } from 'luxon'
import { BaseModel, beforeCreate, beforeDelete, beforeUpdate, column, computed } from '@ioc:Adonis/Lucid/Orm'
import Env from '@ioc:Adonis/Core/Env'

import HttpContext from '@ioc:Adonis/Core/HttpContext'

export default class Slider extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public image: string

  @column()
  public link: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime | null;

  @column()
  public createdBy?: number | null

  @column()
  public updatedBy?: number | null

  @column()
  public deletedBy?: number | null

  @beforeCreate()
  public static async assignCreateBy(data: Slider) {
    const auth = await HttpContext.getOrFail().auth
    data.createdBy = auth.use('api').user!!.id    
  }

  @beforeUpdate()
  public static async assignUpdateBy(data: Slider) {
    const auth = await HttpContext.getOrFail()!!.auth
    data.updatedBy = auth.use('api').user!!.id
  }

  @beforeDelete()
  public static async assignDeleteBy(data: Slider) {
    const auth = await HttpContext.getOrFail()!!.auth
      data.deletedBy = auth.use('api').user!!.id
  }

  @computed()
  public get imageLink() {
    if (this.image && !this.image.includes('http')) {
      return Env.get('URL').replace('0.0.0.0', '127.0.0.1') + 'uploads/sliders/' + this.image
    }
  }
}
