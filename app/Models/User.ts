import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import { column, beforeSave, BaseModel, computed, beforeCreate, beforeUpdate, beforeDelete } from '@ioc:Adonis/Lucid/Orm'
import { SoftDeletes } from '@ioc:Adonis/Addons/LucidSoftDeletes'
import { compose } from '@ioc:Adonis/Core/Helpers'
import Env from '@ioc:Adonis/Core/Env'

import HttpContext from '@ioc:Adonis/Core/HttpContext'

export default class User extends compose(BaseModel, SoftDeletes) {
  @column({ isPrimary: true })
  public id: number

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public image?: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime | null;

  @column()
  public createdBy?: number | null

  @column()
  public updatedBy?: number | null

  @column()
  public deletedBy?: number | null

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @beforeCreate()
  public static async assignCreateBy(data: User) {
    const auth = await HttpContext.getOrFail().auth
    data.createdBy = auth.use('api').user!!.id
  }

  @beforeUpdate()
  public static async assignUpdateBy(data: User) {
    const auth = await HttpContext.getOrFail()!!.auth
    data.updatedBy = auth.use('api').user!!.id
  }

  @beforeDelete()
  public static async assignDeleteBy(data: User) {
    const auth = await HttpContext.getOrFail()!!.auth
    data.deletedBy = auth.use('api').user!!.id
  }

  @computed()
  public get imageLink() {
    if (this.image && !this.image.includes('http')) {
      return Env.get('URL').replace('0.0.0.0', '127.0.0.1') + 'uploads/users/' + this.image
    }
    return 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg'
  }
}
