import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class AppsUser extends BaseModel {
  public static connection = 'apps'
  public static table = 'users'

  @column({ isPrimary: true })
  public usId: number

  @column()
  public usUsername: string

  @column()
  public usName: string

  @column()
  public usEmail: string

  @column()
  public usPhone: string

  @column()
  public usBalance: number

  @column()
  public usData: JSON
}
