import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Categories extends BaseSchema {
  protected tableName = 'categories'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name')
      table.string('slug')
      table.string('image')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamp('created_at', { useTz: true }).notNullable()      
       table.timestamp('updated_at', { useTz: true }).notNullable()
       table.timestamp('deleted_at', { useTz: true }).nullable()
       table.integer('created_by').unsigned().nullable()
       table.integer('updated_by').unsigned().nullable()
       table.integer('deleted_by').unsigned().nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
