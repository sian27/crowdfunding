import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Campaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('title')
      table.string('slug')
      table.integer('category_id').unsigned()
      table.bigInteger('target_donation')
      table.date('max_date')
      table.text('description')
      table.string('image')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamp('created_at', { useTz: true }).notNullable()      
       table.timestamp('updated_at', { useTz: true }).notNullable()
       table.timestamp('deleted_at', { useTz: true }).nullable()
       table.integer('created_by').unsigned().nullable()
       table.integer('updated_by').unsigned().nullable()
       table.integer('deleted_by').unsigned().nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
