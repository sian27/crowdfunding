import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Donaturs extends BaseSchema {
  protected tableName = 'donaturs'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('apps_id').unsigned().nullable()
      table.string('name')
      table.string('email').unique()
      table.timestamp('email_verified_at').nullable()
      table.string('password').nullable()
      table.string('image').nullable()
      table.string('remember_token')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).notNullable()
      table.timestamp('updated_at', { useTz: true }).notNullable()
      table.timestamp('deleted_at', { useTz: true }).nullable()
      table.integer('created_by').unsigned().nullable()
      table.integer('updated_by').unsigned().nullable()
      table.integer('deleted_by').unsigned().nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
