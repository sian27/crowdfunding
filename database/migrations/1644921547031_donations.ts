import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Donations extends BaseSchema {
  protected tableName = 'donations'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('invoice');
      table.integer('campaign_id').unsigned()
      table.integer('donatur_id').unsigned()
      table.bigInteger('amount')
      table.text('pray').nullable();
      table.string('snap_token').nullable();
      table.enum('status', ['pending', 'success', 'expired', 'failed']);

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).notNullable()
      table.timestamp('updated_at', { useTz: true }).notNullable()
      table.timestamp('deleted_at', { useTz: true }).nullable()
      table.integer('created_by').unsigned().nullable()
      table.integer('updated_by').unsigned().nullable()
      table.integer('deleted_by').unsigned().nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
