import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Campaign from 'App/Models/Campaign'

export default class CampaignSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await Campaign.createMany([
      {
        title: 'BANJIR! 99 RT Terendam Banjir Hingga 1.5 M',
        slug: 'banjir-99-rt-terendam-banjir-hingga-15-m',
        categoryId: 3,
        targetDonation: 50000000,
        maxDate: new Date('2022-03-20'),
        description: '<p><span style=\"color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">Tercatat ada</span><span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">&nbsp;26 titik yang mengalami banjir,&nbsp;</span><span style=\"color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">bahkan di wilayah Cipinang Melayu, warga di 6 RT mengungsi sementara akibat rumah terendam banjir.</span></p>\r\n<p><span style=\"color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">Total sudah ada&nbsp;</span><span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">99 RT di Jakarta terendam banjir&nbsp;</span><span style=\"color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">yang sebagian di antaranya</span><span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">&nbsp;mencapai ketinggian 1,5 meter.</span><span style=\"color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">&nbsp;Dilansir dari&nbsp;</span><a style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; text-decoration-line: none; color: #00aeef; transition: all 0.35s ease 0s; background-color: #ffffff; font-family: \'Open Sans\', sans-serif; font-size: 14px;\" href=\"https://www.cnnindonesia.com/nasional/20210219174632-20-608544/99-rt-di-jakarta-tergenang-banjir-694-orang-mengungsi\">CNN Indonesia</a><span style=\"color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">, Pelaksana tugas (Plt) BPBD DKI Jakarta Sabdo Kurnianto menyebutkan wilayah</span><span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">&nbsp;paling terdampak yakni Jakarta Barat dan Jakarta Timur.</span></p>',
        image: 'Xyd2vQYdL3EAlkgzuxBMydWykRksco5rMB5QFSh2.png'
      },
      {
        title: 'Pahala Tak Terputus! Bangun Masjid Terkena Gempa',
        slug: 'pahala-tak-terputus-bangun-masjid-terkena-gempa',
        categoryId: 1,
        targetDonation: 1000000000,
        maxDate: new Date('2022-03-27'),
        description: '<p style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; line-height: 1.5; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\"><span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold;\">Sebagaimana kehilangan rumah, Sulawesi Barat menyimpan luka mendalam ketika masjid-masjid mereka hancur akibat gempa.&nbsp;</span></p>\r\n<p style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; line-height: 1.5; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">Mereka lebih mengutamakan untuk masjid-masjid mereka bisa berdiri kembali daripada rumah sendiri.&nbsp;</p>\r\n<p style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; line-height: 1.5; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">Dini hari itu, 15 Januari 2021, Dusun Sendana, Sulawesi Barat diguncang gempa berkekuatan 6.2 SR. Langit masih gelap. Sebagian warga tengah shalat malam, sebagian lagi terlelap.&nbsp;<span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold;\">Tiba-tiba, bumi bergetar. Makin lama makin keras, hingga tanah luluh lantak. Teriakan takbir dan tangis terdengar bersahutan.&nbsp;</span></p>\r\n<p style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; line-height: 1.5; color: #4a4a4a; font-family: \'Open Sans\', sans-serif; font-size: 14px; background-color: #ffffff;\">Saat matahari terbit dan alam mulai benderang, tampaklah puing-puing bangunan yang hancur, juga isak-tangis warga yang mencari kerabatnya.&nbsp;<span style=\"box-sizing: border-box; -webkit-tap-highlight-color: transparent; font-weight: bold;\">Namun ada pemandangan yang lebih memilukan. Di hadapan mereka, Masjid Ar Rahman, masjid satu-satunya di kampung mereka, porak-poranda diguncang gempa.</span></p>',
        image: '88yH4zp3xtzFskz2RSHlmPAi63t1fhFDXpuqiNDB.png'
      }
    ])
  }
}
