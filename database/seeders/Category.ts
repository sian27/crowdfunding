import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Category from 'App/Models/Category'

export default class CategorySeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await Category.createMany([
      {
        name: 'Rumah Ibadah',
        slug: 'rumah-ibadah',
        image: 'FQARhnigguz1HAG4YKQ065YpuP9ZQCvwiir7gtqR.png',
      },
      {
        name: 'Balita',
        slug: 'balita',
        image: 'Io38XAEAPFYm4caUh87Dtwoq2BrRsg9R8iXpFyKE.png',
      },
      {
        name: 'Bencana Alam',
        slug: 'bencana-alam',
        image: 'y4SvkYTmmjg94CBhgVbtlKvwtUdqTDEa3MfJ0TKN.png',
      }
    ])
  }
}
