import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Donation from 'App/Models/Donation'

export default class DonationSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await Donation.create({
      invoice: 'TRX-2V9AO0P887',
      campaignId: 2,
      donaturId: 1,
      amount: 1000000,
      pray: 'Semoga Masjid-nya cepat selesai dan memberikan keberkahan untuk umat',
      snapToken: 'b04fd6cd-4818-4e4b-bbcd-c8cbd88cc6cd',
      status: 'pending',
    })
  }
}
