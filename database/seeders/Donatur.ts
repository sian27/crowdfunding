import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Donatur from 'App/Models/Donatur'

export default class DonaturSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await Donatur.create({
      name: 'Si Demo',
      email: 'sidemo@mail.com',
      password: 'password',
      image: 'TsTtRWVmj4XkfCZGpzfqF0CKfyoZAMA7BXBDi9m6.png',
    })
  }
}
