import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Slider from 'App/Models/Slider'

export default class SliderSeeder extends BaseSeeder {
  public async run () {
    // Write your database queries inside the run method
    await Slider.createMany([
      {
        image: 'oj7V6eBuELKvqLkxenzoIAoBEyks4zz5aFg1sClf.png',
        link: '#',
      },
      {
        image: '8fmbhBOOrC8SrzrMyjT2Tn3z3Kve2Q6vrV3THqyU.png',
        link: '#',
      }
    ])
  }
}
