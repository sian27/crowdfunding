/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

// Route.get('/', async () => {
//   return { hello: 'world' }
// })

// Frontend
// without auth
Route.group(() => {

  // auth
  Route.post("login", 'LoginController.login')

  // campaign
  Route.get('campaign', 'CampaignsController.index')
  Route.get('campaign/:slug', 'CampaignsController.show')

  // category
  Route.get('category', 'CategoriesController.index')
  Route.get('category/:slug', 'CategoriesController.show')

  // slider
  Route.get('slider', 'SlidersController.index')

  // with auth
  Route.group(() => {
    Route.get('me', "LoginController.me")
    Route.post('logout', "LoginController.logout")
    Route.get('donation', 'DonationsController.index')
    Route.post('donation', 'DonationsController.store')

  }).middleware(['auth:api'])
}).prefix('api/v1').namespace('App/Controllers/Http/Api')


// Backend
Route.group(() => {

}).prefix('api/cms/v1').namespace('App/Controller/Http/Admin')
